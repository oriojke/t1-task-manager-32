package ru.t1.didyk.taskmanager.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.request.DataJsonLoadFasterXmlRequest;
import ru.t1.didyk.taskmanager.enumerated.Role;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-json-load-fasterxml";
    @NotNull
    public static final String DESCRIPTION = "Load data from json file";

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest();
        serviceLocator.getDomainEndpointClient().dataJsonLoadFasterXml(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
