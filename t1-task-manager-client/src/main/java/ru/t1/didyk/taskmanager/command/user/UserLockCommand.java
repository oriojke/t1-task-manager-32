package ru.t1.didyk.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.UserLockRequest;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-lock";
    @NotNull
    public static final String DESCRIPTION = "Lock user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest();
        request.setLogin(login);
        serviceLocator.getUserEndpointClient().lockUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
