package ru.t1.didyk.taskmanager.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.request.DataBinarySaveRequest;
import ru.t1.didyk.taskmanager.enumerated.Role;

public class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-binary-save";
    @NotNull
    public static final String DESCRIPTION = "Save data to binary file";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest();
        serviceLocator.getDomainEndpointClient().dataBinarySave(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
