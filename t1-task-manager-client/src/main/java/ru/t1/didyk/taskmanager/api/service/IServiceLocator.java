package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.client.IEndpointClient;
import ru.t1.didyk.taskmanager.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    AuthEndpointClient getAuthEndpointClient();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

}
