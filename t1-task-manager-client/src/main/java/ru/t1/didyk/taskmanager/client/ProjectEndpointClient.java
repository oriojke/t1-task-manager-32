package ru.t1.didyk.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.client.IProjectEndpointClient;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;

public class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpointClient {

    @Override
    @NotNull
    public ProjectChangeStatusByIdResponse changeStatusById(@NotNull ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @Override
    @NotNull
    public ProjectClearResponse clearProject(@NotNull ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @Override
    @NotNull
    public ProjectCompleteByIdResponse completeById(@NotNull ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @Override
    @NotNull
    public ProjectCompleteByIndexResponse completeByIndex(@NotNull ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @Override
    @NotNull
    public ProjectCreateResponse create(@NotNull ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @Override
    @NotNull
    public ProjectListResponse listProjects(@NotNull ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @Override
    @NotNull
    public ProjectRemoveByIdResponse removeById(@NotNull ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @Override
    @NotNull
    public ProjectRemoveByIndexResponse removeByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @Override
    @NotNull
    public ProjectShowByIdResponse showById(@NotNull ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @Override
    @NotNull
    public ProjectShowByIndexResponse showByIndex(@NotNull ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @Override
    @NotNull
    public ProjectStartByIdResponse startById(@NotNull ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @Override
    @NotNull
    public ProjectStartByIndexResponse startByIndex(@NotNull ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @Override
    @NotNull
    public ProjectUpdateByIdResponse updateById(@NotNull ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @Override
    @NotNull
    public ProjectUpdateByIndexResponse updateByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}
