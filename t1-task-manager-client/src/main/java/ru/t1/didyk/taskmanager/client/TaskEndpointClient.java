package ru.t1.didyk.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.client.ITaskEndpointClient;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;

public class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {
    @Override
    public @NotNull TaskBindToProjectResponse bindToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @Override
    public @NotNull TaskChangeStatusByIdResponse changeStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @Override
    public @NotNull TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @Override
    public @NotNull TaskClearResponse clear(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @Override
    public @NotNull TaskCompleteByIdResponse completeById(@NotNull TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @Override
    public @NotNull TaskCompleteByIndexResponse completeByIndex(@NotNull TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @Override
    public @NotNull TaskCreateResponse create(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @Override
    public @NotNull TaskListResponse listTasks(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @Override
    public @NotNull TaskRemoveByIdResponse removeById(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @Override
    public @NotNull TaskRemoveByIndexResponse removeByIndex(@NotNull TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @Override
    public @NotNull TaskShowByIdResponse showById(@NotNull TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @Override
    public @NotNull TaskShowByIndexResponse showByIndex(@NotNull TaskShowByIndexRequest request) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @Override
    public @NotNull TaskShowByProjectIdResponse showByProjectId(@NotNull TaskShowByProjectIdRequest request) {
        return call(request, TaskShowByProjectIdResponse.class);
    }

    @Override
    public @NotNull TaskStartByIdResponse startById(@NotNull TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @Override
    public @NotNull TaskStartByIndexResponse startByIndex(@NotNull TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @Override
    public @NotNull TaskUnbindFromProjectResponse unbindFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @Override
    public @NotNull TaskUpdateByIdResponse updateById(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @Override
    public @NotNull TaskUpdateByIndexResponse updateByIndex(@NotNull TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }
}
