package ru.t1.didyk.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.client.IUserEndpointClient;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;

public class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {
    @Override
    public @NotNull UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @Override
    public @NotNull UserLockResponse lockUser(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @Override
    public @NotNull UserRegistryResponse register(@NotNull UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @Override
    public @NotNull UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @Override
    public @NotNull UserUnlockResponse unlockUser(@NotNull UserUnlockRequset request) {
        return call(request, UserUnlockResponse.class);
    }

    @Override
    public @NotNull UserUpdateProfileResponse updateProfile(@NotNull UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }
}
