package ru.t1.didyk.taskmanager.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.client.IAuthEndpointClient;
import ru.t1.didyk.taskmanager.dto.request.UserLoginRequest;
import ru.t1.didyk.taskmanager.dto.request.UserLogoutRequest;
import ru.t1.didyk.taskmanager.dto.request.UserViewProfileRequest;
import ru.t1.didyk.taskmanager.dto.response.UserLoginResponse;
import ru.t1.didyk.taskmanager.dto.response.UserLogoutResponse;
import ru.t1.didyk.taskmanager.dto.response.UserViewProfileResponse;

public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    @Override
    public @NotNull UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    public @NotNull UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    public @NotNull UserViewProfileResponse profile(@NotNull UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());
        System.out.println(authEndpointClient.login(new UserLoginRequest("test2", "test2")));
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")));
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());
        authEndpointClient.disconnect();
    }
}
