package ru.t1.didyk.taskmanager.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.client.ISystemEndpointClient;
import ru.t1.didyk.taskmanager.dto.request.ServerAboutRequest;
import ru.t1.didyk.taskmanager.dto.request.ServerVersionRequest;
import ru.t1.didyk.taskmanager.dto.response.ServerAboutResponse;
import ru.t1.didyk.taskmanager.dto.response.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();

    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}
