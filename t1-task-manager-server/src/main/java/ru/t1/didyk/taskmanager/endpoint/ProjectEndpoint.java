package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.endpoint.IProjectEndpoint;
import ru.t1.didyk.taskmanager.api.service.IProjectService;
import ru.t1.didyk.taskmanager.api.service.IServiceLocator;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Project;

import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIdResponse changeStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, projectId, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    @NotNull
    public ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    @NotNull
    public ProjectCompleteByIdResponse completeById(@NotNull final ProjectCompleteByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, projectId, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectCompleteByIndexResponse completeByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(project);
    }

    @Override
    @NotNull
    public ProjectCreateResponse create(@NotNull final ProjectCreateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @NotNull
    public ProjectListResponse listProjects(@NotNull ProjectListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSortType();
        @Nullable final List<Project> projects = getProjectService().findAll(userId, sort);
        System.out.println(userId);
        return new ProjectListResponse(projects);
    }

    @Override
    @NotNull
    public ProjectRemoveByIdResponse removeById(@NotNull ProjectRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project = getProjectService().findOneById(userId, projectId);
        getProjectService().removeById(userId, projectId);
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectRemoveByIndexResponse removeByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        getProjectService().removeByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @Override
    @NotNull
    public ProjectShowByIdResponse showById(@NotNull ProjectShowByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project = getProjectService().findOneById(userId, projectId);
        return new ProjectShowByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectShowByIndexResponse showByIndex(@NotNull ProjectShowByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @Override
    @NotNull
    public ProjectStartByIdResponse startById(@NotNull ProjectStartByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, projectId, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectStartByIndexResponse startByIndex(@NotNull ProjectStartByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(project);
    }

    @Override
    @NotNull
    public ProjectUpdateByIdResponse updateById(@NotNull ProjectUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().updateById(userId, projectId, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    @NotNull
    public ProjectUpdateByIndexResponse updateByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }
}
