package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.User;

public interface IAuthService {

    @Nullable
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

    User check(@Nullable final String login, @Nullable final String password);

}
