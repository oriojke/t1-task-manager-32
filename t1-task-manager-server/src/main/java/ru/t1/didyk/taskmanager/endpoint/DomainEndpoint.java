package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.endpoint.IDomainEndpoint;
import ru.t1.didyk.taskmanager.api.service.IServiceLocator;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;
import ru.t1.didyk.taskmanager.enumerated.Role;

public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @Override
    @NotNull
    public DataBackupLoadResponse dataBackupLoad(@NotNull DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @Override
    @NotNull
    public DataBackupSaveResponse dataBackupSave(@NotNull DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @Override
    @NotNull
    public DataBase64LoadResponse dataBase64Load(@NotNull DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @Override
    @NotNull
    public DataBase64SaveResponse dataBase64Save(@NotNull DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @Override
    @NotNull
    public DataBinaryLoadResponse dataBinaryLoad(@NotNull DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @Override
    @NotNull
    public DataBinarySaveResponse dataBinarySave(@NotNull DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadFasterXmlResponse dataJsonLoadFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadJaxBResponse dataJsonLoadJaxB(@NotNull DataJsonLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveFasterXmlResponse dataJsonSaveFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveJaxBResponse dataJsonSaveJaxB(@NotNull DataJsonSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadFasterXmlResponse dataXmlLoadFasterXml(@NotNull DataXmlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadJaxBResponse dataXmlLoadJaxB(@NotNull DataXmlLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveFasterXmlResponse dataXmlSaveFasterXml(@NotNull DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveJaxBResponse dataXmlSaveJaxB(@NotNull DataXmlSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @Override
    @NotNull
    public DataYamlLoadFasterXmlResponse dataYamlLoadFasterXml(@NotNull DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataYamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataYamlSaveFasterXmlResponse dataYamlSaveFasterXml(@NotNull DataYamlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataYamlLoadFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }
}
