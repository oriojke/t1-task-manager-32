package ru.t1.didyk.taskmanager.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.endpoint.*;
import ru.t1.didyk.taskmanager.api.repository.IProjectRepository;
import ru.t1.didyk.taskmanager.api.repository.ITaskRepository;
import ru.t1.didyk.taskmanager.api.repository.IUserRepository;
import ru.t1.didyk.taskmanager.api.service.*;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.endpoint.*;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.model.User;
import ru.t1.didyk.taskmanager.repository.ProjectRepository;
import ru.t1.didyk.taskmanager.repository.TaskRepository;
import ru.t1.didyk.taskmanager.repository.UserRepository;
import ru.t1.didyk.taskmanager.service.*;
import ru.t1.didyk.taskmanager.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.didyk.taskmanager.command";

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::dataBackupLoad);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::dataBackupSave);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::dataBase64Load);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::dataBase64Save);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::dataBinaryLoad);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::dataBinarySave);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::dataJsonLoadFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::dataJsonLoadJaxB);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::dataJsonSaveFasterXml);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::dataJsonSaveJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::dataXmlLoadFasterXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::dataXmlLoadJaxB);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::dataXmlSaveFasterXml);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::dataXmlSaveJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::dataYamlLoadFasterXml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::dataYamlSaveFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::create);
        server.registry(ProjectListRequest.class, projectEndpoint::listProjects);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateByIndex);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clear);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::create);
        server.registry(TaskListRequest.class, taskEndpoint::listTasks);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showByIndex);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::showByProjectId);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateByIndex);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::register);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUnlockRequset.class, userEndpoint::unlockUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfile);
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@test.ru");
        @NotNull final User user = userService.create("user", "user", "user@test.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST PROJECT 1", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("TEST PROJECT 2", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("TEST PROJECT 3", Status.COMPLETED));
        projectService.add(test.getId(), new Project("TEST PROJECT 4", Status.IN_PROGRESS));

        taskService.create(test.getId(), "TEST TASK 1");
        taskService.create(test.getId(), "TEST TASK 2");
    }

    public void start() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME THE TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        server.start();
    }

    private void stop() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
        server.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
