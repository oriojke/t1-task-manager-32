package ru.t1.didyk.taskmanager.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskUnbindFromProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

}
