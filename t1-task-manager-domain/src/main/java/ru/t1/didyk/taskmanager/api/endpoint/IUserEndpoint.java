package ru.t1.didyk.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;

public interface IUserEndpoint {
    @NotNull UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull UserRegistryResponse register(@NotNull UserRegistryRequest request);

    @NotNull UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull UserUnlockResponse unlockUser(@NotNull UserUnlockRequset request);

    @NotNull UserUpdateProfileResponse updateProfile(@NotNull UserUpdateProfileRequest request);
}
