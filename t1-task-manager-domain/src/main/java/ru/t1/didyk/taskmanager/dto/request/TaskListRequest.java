package ru.t1.didyk.taskmanager.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sortType;

}
