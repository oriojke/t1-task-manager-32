package ru.t1.didyk.taskmanager.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TaskClearRequest extends AbstractUserRequest {
}
